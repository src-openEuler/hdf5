%global with_mpich 1
%global with_openmpi 1
%if %{with_mpich}
%global mpi_list mpich
%endif
%if %{with_openmpi}
%global mpi_list %{?mpi_list} openmpi
%endif

%global so_version 310

Name: hdf5
Version: 1.14.5
Release: 1
Summary: A data model, library, and file format for storing and managing data
License: BSD-3-Clause

URL:     https://www.hdfgroup.org/solutions/hdf5/
Source0: https://support.hdfgroup.org/releases/hdf5/v1_14/v1_14_5/downloads/hdf5-1.14.5.tar.gz
Source1: h5comp

Patch0: hdf5-wrappers.patch

BuildRequires: gcc, gcc-c++
BuildRequires: krb5-devel, openssl-devel, zlib-devel, gcc-gfortran, time
BuildRequires: automake libtool
BuildRequires: openssh-clients
BuildRequires: libaec-devel
BuildRequires: environment-modules

%description
HDF5 is a data model, library, and file format for storing and managing data. It supports an unlimited variety of datatypes, and is designed for flexible and efficient I/O and for high volume and complex data. HDF5 is portable and is extensible, allowing applications to evolve in their use of HDF5. The HDF5 Technology suite includes tools and applications for managing, manipulating, viewing, and analyzing data in the HDF5 format.

%package devel
Summary: HDF5 development files
Provides: hdf5-static = %{version}-%{release}
Obsoletes: hdf5-static < %{version}-%{release}
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: libaec-devel%{?_isa}
Requires: zlib-devel%{?_isa}
Requires: gcc-gfortran%{?_isa}

%description devel
HDF5 development headers and libraries.

%if %{with_mpich}
%package mpich
Summary: HDF5 mpich libraries
BuildRequires: mpich-devel
Provides: %{name}-mpich2 = %{version}-%{release}
Obsoletes: %{name}-mpich2 < %{version}-%{release}

%description mpich
HDF5 parallel mpich libraries


%package mpich-devel
Summary: HDF5 mpich development files
Requires: %{name}-mpich%{?_isa} = %{version}-%{release}
Requires: libaec-devel%{?_isa}
Requires: zlib-devel%{?_isa}
Requires: mpich-devel%{?_isa}
Provides: %{name}-mpich2-devel = %{version}-%{release}
Obsoletes: %{name}-mpich2-devel < %{version}-%{release}

%description mpich-devel
HDF5 parallel mpich development files

%package mpich-static
Summary: HDF5 mpich static libraries
Requires: %{name}-mpich-devel%{?_isa} = %{version}-%{release}
Provides: %{name}-mpich2-static = %{version}-%{release}
Obsoletes: %{name}-mpich2-static < %{version}-%{release}

%description mpich-static
HDF5 parallel mpich static libraries
%endif

%if %{with_openmpi}
%package openmpi
Summary: HDF5 openmpi libraries
BuildRequires: openmpi-devel

%description openmpi
HDF5 parallel openmpi libraries


%package openmpi-devel
Summary: HDF5 openmpi development files
Requires: %{name}-openmpi%{?_isa} = %{version}-%{release}
Requires: libaec-devel%{?_isa}
Requires: zlib-devel%{?_isa}
Requires: openmpi-devel%{?_isa}

%description openmpi-devel
HDF5 parallel openmpi development files


%package openmpi-static
Summary: HDF5 openmpi static libraries
Requires: %{name}-openmpi-devel%{?_isa} = %{version}-%{release}

%description openmpi-static
HDF5 parallel openmpi static libraries
%endif

%prep
%autosetup -n %{name}-%{version} -p1

sed -i -e '/^STATIC_AVAILABLE=/s/=.*/=no/' */*/h5[cf]*.in
autoreconf -f -i

sed -e 's|-O -finline-functions|-O3 -finline-functions|g' -i config/gnu-flags

%build
%global _configure ../configure
%global configure_opts \\\
  --disable-silent-rules \\\
  --enable-fortran \\\
  --enable-hl \\\
  --enable-shared \\\
  --with-szlib \\\
  --enable-mirror-vfd \\\
  --disable-nonstandard-feature-float16 \\\
%{nil}

export CC=gcc
export CXX=g++
export F9X=gfortran
export LDFLAGS="%{__global_ldflags} -fPIC -Wl,-z,now -Wl,--as-needed"
mkdir build
pushd build
ln -s ../configure .
%configure \
  %{configure_opts} \
  --enable-cxx
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool
make %{?_smp_mflags} LDFLAGS="%{__global_ldflags} -fPIC -Wl,-z,now -Wl,--as-needed" FCFLAGS="$FCFLAGS -fallow-argument-mismatch -fallow-invalid-boz"
popd

export CC=mpicc
export CXX=mpicxx
export F9X=mpif90
export LDFLAGS="%{__global_ldflags} -fPIC -Wl,-z,now -Wl,--as-needed"
for mpi in %{?mpi_list}
do
  mkdir $mpi
  pushd $mpi
  module load mpi/$mpi-%{_arch}
  ln -s ../configure .
  %configure \
    %{configure_opts} \
    FCFLAGS="$FCFLAGS -I$MPI_FORTRAN_MOD_DIR -fallow-argument-mismatch -fallow-invalid-boz" \
    --enable-parallel \
    --exec-prefix=%{_libdir}/$mpi \
    --libdir=%{_libdir}/$mpi/lib \
    --bindir=%{_libdir}/$mpi/bin \
    --sbindir=%{_libdir}/$mpi/sbin \
    --includedir=%{_includedir}/$mpi-%{_arch} \
    --datarootdir=%{_libdir}/$mpi/share \
    --mandir=%{_libdir}/$mpi/share/man
  sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool
  make %{?_smp_mflags} LDFLAGS="%{__global_ldflags} -fPIC -Wl,-z,now -Wl,--as-needed"
  module purge
  popd
done

%install
%make_install -C build
%delete_la
mkdir -p ${RPM_BUILD_ROOT}%{_fmoddir}
mv ${RPM_BUILD_ROOT}%{_includedir}/*.mod ${RPM_BUILD_ROOT}%{_fmoddir}

for mpi in %{?mpi_list}
do
  module load mpi/$mpi-%{_arch}
  make -C $mpi install DESTDIR=${RPM_BUILD_ROOT}
  rm $RPM_BUILD_ROOT/%{_libdir}/$mpi/lib/*.la
  #Fortran modules
  mkdir -p ${RPM_BUILD_ROOT}${MPI_FORTRAN_MOD_DIR}
  mv ${RPM_BUILD_ROOT}%{_includedir}/${mpi}-%{_arch}/*.mod ${RPM_BUILD_ROOT}${MPI_FORTRAN_MOD_DIR}/
  module purge
done

%ifarch x86_64
sed -i -e s/H5pubconf.h/H5pubconf-64.h/ ${RPM_BUILD_ROOT}%{_includedir}/H5public.h
mv ${RPM_BUILD_ROOT}%{_includedir}/H5pubconf.h \
   ${RPM_BUILD_ROOT}%{_includedir}/H5pubconf-64.h
for x in h5c++ h5cc h5fc
do
  mv ${RPM_BUILD_ROOT}%{_bindir}/${x} \
     ${RPM_BUILD_ROOT}%{_bindir}/${x}-64
  install -m 0755 %SOURCE1 ${RPM_BUILD_ROOT}%{_bindir}/${x}
done
%else
sed -i -e s/H5pubconf.h/H5pubconf-32.h/ ${RPM_BUILD_ROOT}%{_includedir}/H5public.h
mv ${RPM_BUILD_ROOT}%{_includedir}/H5pubconf.h \
   ${RPM_BUILD_ROOT}%{_includedir}/H5pubconf-32.h
for x in h5c++ h5cc h5fc
do
  mv ${RPM_BUILD_ROOT}%{_bindir}/${x} \
     ${RPM_BUILD_ROOT}%{_bindir}/${x}-32
  install -m 0755 %SOURCE1 ${RPM_BUILD_ROOT}%{_bindir}/${x}
done
%endif

mkdir -p ${RPM_BUILD_ROOT}/%{_rpmmacrodir}
cat > ${RPM_BUILD_ROOT}/%{_rpmmacrodir}/macros.hdf5 <<EOF
# HDF5 version is
%%_hdf5_version %{version}
EOF

%check
make %{?_smp_mflags} -C build check
%ldconfig_scriptlets

%files
%license COPYING
%doc release_docs/RELEASE.txt
%doc release_docs/HISTORY*.txt
%{_bindir}/h5clear
%{_bindir}/h5copy
%{_bindir}/h5debug
%{_bindir}/h5delete
%{_bindir}/h5diff
%{_bindir}/h5dump
%{_bindir}/h5format_convert
%{_bindir}/h5fuse
%{_bindir}/h5import
%{_bindir}/h5jam
%{_bindir}/h5ls
%{_bindir}/h5mkgrp
%{_bindir}/h5perf_serial
%{_bindir}/h5repack
%{_bindir}/h5repart
%{_bindir}/h5stat
%{_bindir}/h5unjam
%{_bindir}/h5watch
%{_bindir}/mirror_server
%{_bindir}/mirror_server_stop
%{_libdir}/libhdf5.so.%{so_version}*
%{_libdir}/libhdf5_cpp.so.%{so_version}*
%{_libdir}/libhdf5_fortran.so.%{so_version}*
%{_libdir}/libhdf5hl_fortran.so.%{so_version}*
%{_libdir}/libhdf5_hl.so.%{so_version}*
%{_libdir}/libhdf5_hl_cpp.so.%{so_version}*

%files devel
%{_bindir}/h5c++*
%{_bindir}/h5cc*
%{_bindir}/h5fc*
%{_bindir}/h5redeploy
%{_includedir}/*.h
%{_includedir}/H5config_f.inc
%{_libdir}/*.so
%{_libdir}/*.settings
%{_fmoddir}/*.mod
%{_libdir}/*.a
%{_rpmmacrodir}/macros.hdf5

%if %{with_mpich}
%files mpich
%license COPYING
%doc release_docs/RELEASE.txt
%doc release_docs/HISTORY*.txt
%{_libdir}/mpich/bin/h5clear
%{_libdir}/mpich/bin/h5copy
%{_libdir}/mpich/bin/h5debug
%{_libdir}/mpich/bin/h5delete
%{_libdir}/mpich/bin/h5diff
%{_libdir}/mpich/bin/h5dump
%{_libdir}/mpich/bin/h5format_convert
%{_libdir}/mpich/bin/h5fuse
%{_libdir}/mpich/bin/h5import
%{_libdir}/mpich/bin/h5jam
%{_libdir}/mpich/bin/h5ls
%{_libdir}/mpich/bin/h5mkgrp
%{_libdir}/mpich/bin/h5redeploy
%{_libdir}/mpich/bin/h5repack
%{_libdir}/mpich/bin/h5perf
%{_libdir}/mpich/bin/h5perf_serial
%{_libdir}/mpich/bin/h5repart
%{_libdir}/mpich/bin/h5stat
%{_libdir}/mpich/bin/h5unjam
%{_libdir}/mpich/bin/h5watch
%{_libdir}/mpich/bin/mirror_server
%{_libdir}/mpich/bin/mirror_server_stop
%{_libdir}/mpich/bin/ph5diff
%{_libdir}/mpich/lib/*.so.%{so_version}*

%files mpich-devel
%{_includedir}/mpich-%{_arch}
%{_fmoddir}/mpich/*.mod
%{_libdir}/mpich/bin/h5pcc
%{_libdir}/mpich/bin/h5pfc
%{_libdir}/mpich/lib/lib*.so
%{_libdir}/mpich/lib/lib*.settings

%files mpich-static
%{_libdir}/mpich/lib/*.a
%endif

%if %{with_openmpi}
%files openmpi
%license COPYING
%doc release_docs/RELEASE.txt
%doc release_docs/HISTORY*.txt
%{_libdir}/openmpi/bin/h5clear
%{_libdir}/openmpi/bin/h5copy
%{_libdir}/openmpi/bin/h5debug
%{_libdir}/openmpi/bin/h5delete
%{_libdir}/openmpi/bin/h5diff
%{_libdir}/openmpi/bin/h5dump
%{_libdir}/openmpi/bin/h5format_convert
%{_libdir}/openmpi/bin/h5fuse
%{_libdir}/openmpi/bin/h5import
%{_libdir}/openmpi/bin/h5jam
%{_libdir}/openmpi/bin/h5ls
%{_libdir}/openmpi/bin/h5mkgrp
%{_libdir}/openmpi/bin/h5perf
%{_libdir}/openmpi/bin/h5perf_serial
%{_libdir}/openmpi/bin/h5redeploy
%{_libdir}/openmpi/bin/h5repack
%{_libdir}/openmpi/bin/h5repart
%{_libdir}/openmpi/bin/h5stat
%{_libdir}/openmpi/bin/h5unjam
%{_libdir}/openmpi/bin/h5watch
%{_libdir}/openmpi/bin/mirror_server
%{_libdir}/openmpi/bin/mirror_server_stop
%{_libdir}/openmpi/bin/ph5diff
%{_libdir}/openmpi/lib/*.so.%{so_version}*

%files openmpi-devel
%{_includedir}/openmpi-%{_arch}
%{_fmoddir}/openmpi/*.mod
%{_libdir}/openmpi/bin/h5pcc
%{_libdir}/openmpi/bin/h5pfc
%{_libdir}/openmpi/lib/lib*.so
%{_libdir}/openmpi/lib/lib*.settings

%files openmpi-static
%{_libdir}/openmpi/lib/*.a
%endif

%changelog
* Wed Oct 30 2024 yaoxin <yao_xin001@hoperun.com> - 1.14.5-1
- Update to 1.14.5:
  * Added signed Windows msi binary and signed Apple dmg binary files.
  * Moved examples to the HDF5Examples folder in the source tree.
  * Added support for using zlib-ng package as the zlib library
  * Disable CMake UNITY_BUILD for hdf5
  * Removed "function/code stack" debugging configuration option
  * Added configure options for enabling/disabling non-standard programming language features
  * Added the CMake variable HDF5_ENABLE_ROS3_VFD to the HDF5 CMake config
    file hdf5-config.cmake. This allows it to easily detect if the library
    has been built with or without read-only S3 functionality.
  * Fixed many CVE:
    CVE-2024-29157,CVE-2024-29158,CVE-2024-29159,CVE-2024-29160,CVE-2024-29161,CVE-2024-29162
    CVE-2024-29163,CVE-2024-29164,CVE-2024-29165,CVE-2024-29166,CVE-2024-32605,CVE-2024-32606
    CVE-2024-32607,CVE-2024-32608,CVE-2024-32609,CVE-2024-32610,CVE-2024-32611,CVE-2024-32612
    CVE-2024-32613,CVE-2024-32614,CVE-2024-32615,CVE-2024-32616,CVE-2024-32617,CVE-2024-32618
    CVE-2024-32619,CVE-2024-32620,CVE-2024-32621,CVE-2024-32622,CVE-2024-32623,CVE-2024-32624
    CVE-2024-33873,CVE-2024-33874,CVE-2024-33875,CVE-2024-33876,CVE-2024-33877,CVE-2018-13871
    CVE-2018-13875,CVE-2018-14034

* Wed Dec 20 2023 wangkai <13474090681@163.com> - 1.12.1-6
- Add disable-hltools flag to fix CVE-2018-17433, CVE-2018-17436, CVE-2020-10809

* Thu Aug 31 2023 xu_ping <707078654@qq.com> - 1.12.1-5
- Add BuildRequires environment-modules

* Tue Apr 11 2023 liyanan <thistleslyn@163.com> - 1.12.1-4
- Preserve the rpath of the hdf5-openmpi and hdf5-mpich subpackages

* Mon Apr 10 2023 xu_ping <707078654@qq.com> - 1.12.1-3
- fix some commands failed

* Tue Mar 28 2023 yaoxin <yaoxin30@h-partners.com> - 1.12.1-2
- Fix CVE-2018-13867,CVE-2018-14031,CVE-2018-16438,CVE-2019-8396,CVE-2020-10812 and CVE-2021-37501

* Thu Jun 23 2022 wulei <wulei80@h-partners.com> - 1.12.1-1
- Upgrade to 1.12.1

* Tue May 10 2022 Ge Wang <wangge20@h-partner.com> - 1.8.20-15
- License compliance rectification

* Fri Sep 10 2021 wangyue <wangyue92@huawei.com> - 1.8.20-14
- fix rpath error

* Mon Aug 2 2021 liushaofei <liushaofei5@huawei.com> - 1.8.20-13
- fix incorrect arguments format

* Thu Mar 25 2021 maminjie <maminjie1@huawei.com> - 1.8.20-12
- Support parallel compilation

* Fri Jan 15 2021 yanan li <liyanan32@huawei.com> - 1.8.20-11
- add sub packages hdf5-openmpi-static, hdf5-openmpi-devel, hdf5-openmpi,
- hdf5-mpich-static, hdf5-mpich-devel, hdf5-mpich

* Mon Dec 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.8.20-10
- fix CVE-2017-17506 CVE-2018-17432 CVE-2018-17435 CVE-2018-13869 CVE-2018-13870 CVE-2018-13873

* Mon Nov 9 2020 wangxiao <wangxiao65@huawei.com> - 1.8.20-9
- fix CVE-2018-17233 CVE-2018-17234 CVE-2018-17237 CVE-2018-17434 CVE-2018-17437 CVE-2018-17438

* Tue Sep 15 2020 shaoqiang kang <kangshaoqiang1@openeuler.org> - 1.8.20-8
- Modify source

* Tue Oct 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.20-7
- Package init
